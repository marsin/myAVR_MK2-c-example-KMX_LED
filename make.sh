#!/bin/bash
# myAVR MK2 example: SMX LED
#   - example program for Switch Matrix input and LED output
# Copyright (c) 2016 Martin Singer <martin.singer@web.de>

avr-g++ main.cc -mmcu=atmega8 -o fw.elf && \
	avr-objcopy -O ihex fw.elf fw.hex && \
	sudo avrdude -p m8 -c avr911 -P /dev/ttyUSB0 -U flash:w:fw.hex:i

