/**********************************************************************
 myAVR MK2 example: SMX LED
   - example program for Switch Matrix input and LED output
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 *********************************************************************/

/**********************************************************************
 Board:       MyAVR MK2
 Controller:  ATmega8L
 Function:    Polls the switches and counts LED up and down in binary
 Circuit:     PortB.0 := ROW0    driver -- blue cables
              PortB.1 := ROW1
              PortB.2 := ROW2
              PortB.3 := ROW3
              PortB.4 := ROW4
              PortC.0 := COL0    reader -- white cables
              PortC.1 := COL1
              PortC.2 := COL2
              PortC.3 := COL3
              PortC.4 := COL4
              PortD.5 := LED_GREEN  - signs 1
              PortD.6 := LED_ORANGE - signs 2
              PortD.7 := LED_RED    - signs 4
 Description:
    Short:
    Fuer jede einzelne Taste der Tastenmatrix werden die Flanken
    (Zustandswechsel) erkannt und nach einem Zustandswechsel die
    jeweilige Zeile und Spalte der Taste durch LED-Signale angezeigt.

    Circuit:
    * Am MC wird eine Tastenmatrix angeschlossen.
    * Jede Taste ist mit einer Entkopplungsdiode versehen
        um Ghosting zu verhindern.
    * Da die Pull-Up-Widerstaende des MC verwendet werden sollen,
        sind die Tasten Low-Aktiv.
    * PortB wird als Treiber-Port verwendet.
        Das bedeutet, dass seine Pins als Ausgang fungieren
        und im inaktiven Zustand auf HIGH (Low-Activ) gesetzt sind.
    * PortC wird als Leser-Port verwendet.
        Das bedeutet, dass seine Pins als Eingang fungieren.
        Die Pull-Up-Widersaende diese Ports werden aktiviert.
    * An PortD sind LEDs angeschlossen und zeigen die gedrueckte Taste
        mit jeweis einem Signal fuer die Zeile und die Spalte an.

    Polling Function:
    Es wird am Treiber-Ausgang (PORTB) ein Zeilen-Pin auf LOW gesetzt
    Nun wird am Lese-Eingang (PORTC) fuer jeden einzelnen Spalten-Pin
    geprueft, ob er LOW ist. Dies kann nur der Fall sein, wenn die
    gepruefte Zeile und Spalte mit dem Taster verbunden wurden.
    Durch die Entkopplungsdiode ist kein Fehlschluss moeglich
    und die Taste eindeutig bestimmt.
    Dies wird fuer alle Zeilen durchgefuerht und stetig wiederholt.
 **********************************************************************/

#define F_CPU 3686400ul

#include <avr/io.h>
#include <avr/interrupt.h>


// Precompiler definitions

// Define Registers
#define DDR_DRIVER  DDRB     // driver register
#define PORT_DRIVER PORTB
#define DDR_READER  DDRC     // reader register
#define PORT_READER PORTC
#define PIN_READER  PINC
#define DDR_LED     DDRD     // TEST LED register
#define PORT_LED    PORTD

// Width of the column and of the row port (max. is port width e.g. 8)
#define ROWS 5
#define COLS 5

// Conditions and Edges
#define COND_UP   0 // key condition is released
#define COND_DOWN 1 // key condition is pressed

#define EDGE_NONE 0 // key don't changes condtition
#define EDGE_FALL 1 // key changes condition from released (up)   to pressed  (down)
#define EDGE_RISE 2 // key changes condition from  pressed (down) to released (up)

#define DEBOUNCE 5  // the debounce ISR counts it to 0


// Global Variables
//static unsigned char
//    table_cond[ROWS][COLS] = { COND_UP }; // key condition table


// ISR: Timer0


// Function
void
init()
{
    // Row-Driver
    DDR_DRIVER  |=  ( (1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3) | (1<<PB4) ); // Output
    PORT_DRIVER |=  ( (1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3) | (1<<PB4) ); // Init port HIGH

    // Column-Reader
    DDR_READER  &= ~( (1<<PC0) | (1<<PC1) | (1<<PC2) | (1<<PC3) | (1<<PC4) ); // Input
    PORT_READER |=  ( (1<<PC0) | (1<<PC1) | (1<<PC2) | (1<<PC3) | (1<<PC4) ); // Pull-Up resistors

    // TEST LEDs
    DDR_LED  |=  ( (1<<PD5) | (1<<PD6) | (1<<PD7) );   // Output
    PORT_LED &= ~( (1<<PD5) | (1<<PD6) | (1<<PD7) );   // Init port LOW

    return;
}


// TEST Function
void led_signal( unsigned char state )
{
    switch ( state ) {
        case 0:
            PORT_LED &= ~( (1<<PB7) | (1<<PB6) | (1<<PB5) );
            break;
        case 1:
            PORT_LED |=  ( (1<<PB5) );
            PORT_LED &= ~( (1<<PB7) | (1<<PB6) );
            break;
        case 2:
            PORT_LED |=  ( (1<<PB6) );
            PORT_LED &= ~( (1<<PB7) | (1<<PB5) );
            break;
        case 3:
            PORT_LED |=  ( (1<<PB6) | (1<<PB5) );
            PORT_LED &= ~( (1<<PB7) );
            break;
        case 4:
            PORT_LED |=  ( (1<<PB7) );
            PORT_LED &= ~( (1<<PB6) | (1<<PB5) );
            break;
        case 5:
            PORT_LED |=  ( (1<<PB7) | (1<<PB5) );
            PORT_LED &= ~( (1<<PB6) );
            break;
        case 6:
            PORT_LED |=  ( (1<<PB7) | (1<<PB6) );
            PORT_LED &= ~( (1<<PB5) );
            break;
        case 7:
            PORT_LED |=  ( (1<<PB7) | (1<<PB6) | (1<<PB5) );
            break;
        default:
            PORT_LED &= ~( (1<<PB7) | (1<<PB6) | (1<<PB5) );
//            PORT_LED |=  ( (1<<PB7) | (1<<PB6) | (1<<PB5) );
            break;
    }

    return;
}


// TEST Function
void
show_switch(
    const unsigned char r,
    const unsigned char c )
{
    unsigned long int i=0;

    // 1st signal: ROW
    led_signal( r+1 );
    for ( i=0; i<100000; ++i );

    // Separator singal
    led_signal( 0 );
    for ( i=0; i<50000; ++i );

    // 2nd signal: COLUMN
    led_signal( c+1 );
    for ( i=0; i<100000; ++i );

    // End signal
    led_signal( 0 );
    for ( i=0; i<50000; ++i );
    led_signal( 7 );
    for ( i=0; i<5000; ++i );
    led_signal( 0 );
    for ( i=0; i<5000; ++i );
    led_signal( 7 );
    for ( i=0; i<5000; ++i );
    led_signal( 0 );
    for ( i=0; i<50000; ++i );

    led_signal( 0 );
    return;
}


// Function


// Function
void
push_scancode(
    const unsigned char row,
    const unsigned char col,
    const unsigned char edge )
{
    show_switch( row, col );
    return;
}


// Function
unsigned char
get_edge(
    const unsigned char row,
    const unsigned char col,
    const unsigned char cond )
{
    static unsigned char
//        table_bounce[ROWS][COLS] = { 255 },     // key debounce table
        table_cond[ROWS][COLS] = { COND_UP };   // key condition table
    unsigned char
        edge = EDGE_NONE;

    if ( cond != table_cond[row][col] ) {
        switch ( cond ) {
            case COND_UP:
                edge = EDGE_RISE;
                break;
            case COND_DOWN:
                edge = EDGE_FALL;
                break;
            default:
                // ERROR handling:
                // - returns default value
                // - don't saves condition implizit
                return EDGE_NONE;
                break;
        }

        // save condition
        table_cond[row][col] = cond;
    }

    return edge;
}


// Function
unsigned char
get_cond(
    const unsigned char row,
    const unsigned char col )
{
    unsigned char
        cond = COND_UP;
    volatile uint8_t
        pin_reader = 0xff;

    PORT_DRIVER = ~(1<<row);
    pin_reader = ~PIN_READER;
    pin_reader &= 0x1f;    // 0001 1111 masks the upper 3 pins out (not connected for TEST)

    if ( pin_reader & (1<<col) ) {
        cond = COND_DOWN;
    }

    return cond;
}


// Function
void
poll()
{
    unsigned char
        cond = COND_UP,
        edge = EDGE_NONE,
        row = 0,
        col = 0;

    while ( 1 ) {
        for ( row=0; row<ROWS; ++row ) {
            for ( col=0; col<COLS; ++col ) {
                cond = get_cond( row, col );
                edge = get_edge( row, col, cond );

                if ( EDGE_RISE == edge || EDGE_FALL == edge ) {
                    push_scancode( row, col, edge );
                }
            }
        }
    }

    return;
}


/*
// Function
void
poll()
{
    unsigned char
        cond = COND_UP,
        edge = EDGE_NONE,
        row = 0,
        col = 0;
    volatile uint8_t
        reg_read = 0xff;

    while ( 1 ) {
        for ( row=0; row<ROWS; ++row ) {
            PORT_DRIVER = ~(1<<row);
            reg_read = ~PIN_READER;
            reg_read &= 0x1f;    // 0001 1111 masks the upper 3 pins out (not connected for TEST)

            for ( col=0; col<COLS; ++col ) {

                if ( reg_read & (1<<col) ) {
                    cond = COND_DOWN;
                }
                else {
                    cond = COND_UP;
                }

                edge = get_edge( row, col, cond );
                if ( EDGE_RISE == edge || EDGE_FALL == edge ) {
                    push_scancode( row, col, edge );
                }
            }
        }
    }

    return;
}
*/


// Main
int main()
{
    init();
    poll();

    return 0;
}

